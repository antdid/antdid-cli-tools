﻿using McMaster.NETCore.Plugins;
using Spectre.Console;

namespace Antdid.Cli.Tools.Commands
{
    internal class PluginManagerCommand : CliCommand
    {
        public readonly List<CliCommand> Plugins = [];
        public PluginManagerCommand() : base()
        {
            var cmd = this;
            cmd.Name = "plugin";
            cmd.AddName("plugins");
            cmd.Description = "Managing plugins";

            cmd.OnExecute(() => Cli.ExecuteHelp(cmd));

            LoadPlugins();

            cmd.Command("ls", cmdList =>
            {
                cmdList.AddName("list");
                cmdList.Description = "List installed plugins";

                cmdList.OnExecute(() =>
                {
                    if (Plugins.Count == 0)
                    {
                        Console.WriteLine("No plugins installed.");
                    }
                    else
                    {
                        var table = new Table
                        {
                            Title = new TableTitle("List of installed plugins")
                        };

                        table.AddColumn("Name");
                        table.AddColumn("Version");

                        foreach (CliCommand plugin in Plugins)
                        {
                            table.AddRow(plugin.FullName ?? plugin.Name!, plugin.ShortVersionGetter?.Invoke() ?? "");
                        }

                        AnsiConsole.Write(table);
                    }
                    return 0;
                });
            });
        }

        private void LoadPlugins()
        {
            var loaders = new List<PluginLoader>();

            // create plugin loaders
            var pluginsDir = Path.Combine(Cli.GetPathWorkDir(), "plugins");
            if (!Directory.Exists(pluginsDir))
            {
                Directory.CreateDirectory(pluginsDir);
            }
            foreach (var dir in Directory.GetDirectories(pluginsDir))
            {
                var dirName = Path.GetFileName(dir);
                var pluginDll = Path.Combine(dir, dirName + ".dll");
                if (File.Exists(pluginDll))
                {
                    var loader = PluginLoader.CreateFromAssemblyFile(
                        pluginDll,
                        sharedTypes: [typeof(CliCommand), typeof(CliInfo)]);
                    loaders.Add(loader);
                }
            }

            // Create an instance of plugin types
            foreach (var loader in loaders)
            {
                foreach (var pluginType in loader
                    .LoadDefaultAssembly()
                    .GetTypes()
                    .Where(t => typeof(CliCommand).IsAssignableFrom(t) && t.IsPublic && !t.IsAbstract))
                {
                    // This assumes the implementation of CliCommand has a one parameter constructor
                    if (Activator.CreateInstance(pluginType, [true]) is CliCommand plugin)
                    {
                        Plugins.Add(plugin);
                    }
                }
            }
        }
    }
}
