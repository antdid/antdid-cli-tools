﻿using McMaster.Extensions.CommandLineUtils;

namespace Antdid.Cli.Tools.Commands
{
    internal class GuidCommand : CliCommand
    {
        public GuidCommand()
        {
            var cmd = this;
            cmd.Name = "guid";
            cmd.Description = "Generate a guid";

            cmd.ExtendedHelpText = GetHelpMessage();

            var optUpper = cmd.Option("-u|--upper", "Uppercase", CommandOptionType.NoValue);
            var optFormat = cmd.Option("-f|--format", "Format guid", CommandOptionType.SingleValue);
            optFormat.DefaultValue = "D";

            cmd.OnExecute(() =>
            {
                var guid = Guid.NewGuid().ToString(optFormat.Value());

                if (optUpper.HasValue())
                {
                    guid = guid.ToUpperInvariant();
                }

                Console.WriteLine(guid);
                return 0;
            });
        }

        #region Private 

        private static string GetHelpMessage()
        {
            return @"
Format:
  Specifier	    Format of return value
    N	        32 digits:
    00000000000000000000000000000000

    D	        32 digits separated by hyphens:
    00000000-0000-0000-0000-000000000000

    B	        32 digits separated by hyphens, enclosed in braces:
    {00000000-0000-0000-0000-000000000000}

    P	        32 digits separated by hyphens, enclosed in parentheses:
    (00000000-0000-0000-0000-000000000000)

    X	        Four hexadecimal values enclosed in braces, where the fourth value is a subset of eight hexadecimal values that is also enclosed in braces:
    {0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}}
";
        }


        #endregion Private
    }
}
