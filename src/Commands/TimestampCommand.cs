﻿using McMaster.Extensions.CommandLineUtils;

namespace Antdid.Cli.Tools.Commands
{
    internal class TimestampCommand : CliCommand
    {
        public TimestampCommand()
        {
            var cmd = this;
            cmd.Name = "tt";
            cmd.AddName("timestamp");
            cmd.Description = "Convert timestamp to date";

            var timestampArg = cmd.Argument<long>("timestamp", "Timestamp for conversion", false).IsRequired();

            cmd.OnExecute(() =>
            {
                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(timestampArg.ParsedValue);
                Console.WriteLine(dateTimeOffset.DateTime);
                return 0;
            });
        }
    }
}
