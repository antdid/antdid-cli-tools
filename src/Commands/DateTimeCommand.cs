﻿using McMaster.Extensions.CommandLineUtils;

namespace Antdid.Cli.Tools.Commands
{
    internal class DateTimeCommand : CliCommand
    {
        public DateTimeCommand()
        {
            var cmd = this;
            cmd.Name = "dt";
            cmd.AddName("date");
            cmd.AddName("datetime");
            cmd.Description = "Display current DateTime";

            var optUtc = cmd.Option("-u|--utc", "UTC Format", CommandOptionType.NoValue);

            cmd.Command("tt", cmdNow =>
            {
                cmdNow.AddName("timestamp");
                cmdNow.Description = "Get timestamp from DateTime.Now";

                cmdNow.OnExecute(() =>
                {
                    DateTimeOffset dateTimeOffset = DateTimeOffset.Now;
                    Console.WriteLine(dateTimeOffset.ToUnixTimeMilliseconds());
                    return 0;
                });
            });

            cmd.OnExecute(() =>
            {
                if (optUtc.HasValue())
                {
                    Console.WriteLine(DateTime.UtcNow);
                }
                else
                {
                    Console.WriteLine(DateTime.Now);
                }
                return 0;
            });
        }
    }
}
