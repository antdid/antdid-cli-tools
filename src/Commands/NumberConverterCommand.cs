﻿using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;

namespace Antdid.Cli.Tools.Commands
{
    internal class NumberConverterCommand : CliCommand
    {
        public NumberConverterCommand()
        {
            var cmd = this;
            cmd.Name = "nb";
            cmd.Description = "Convert a number";

            var argNumber = cmd.Argument("number", "Number to convert", false).IsRequired();
            var optBase = cmd.Option<int>("-b|--base", "The base of the number, which must be 2, 8, 10, or 16.", CommandOptionType.SingleValue);
            optBase.DefaultValue = 10;

            cmd.OnExecute(() =>
            {
                var number = Convert.ToInt64(argNumber.Value, optBase.ParsedValue);

                var numberBinary = Convert.ToString(number, 2);
                var numberOctal = Convert.ToString(number, 8);
                var numberDecimal = Convert.ToString(number, 10);
                var numberHex = Convert.ToString(number, 16);

                var table = new Table();
                table.AddColumn("Binary");
                table.AddColumn("Octal");
                table.AddColumn("Decimal");
                table.AddColumn("Hexadecimal");

                var columnsResult = new List<Text>()
                {
                    new Text(numberBinary.ToString()),
                    new Text(numberOctal.ToString()),
                    new Text(numberDecimal.ToString()),
                    new Text(numberHex.ToString())
                };

                table.AddRow(columnsResult);

                Console.WriteLine();
                AnsiConsole.Write(table);

                return 0;
            });
        }
    }
}
