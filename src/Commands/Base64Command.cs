﻿using System.Text;
using Antdid.Cli.Helpers;
using McMaster.Extensions.CommandLineUtils;

namespace Antdid.Cli.Tools.Commands
{
    internal class Base64Command : CliCommand
    {
        public Base64Command()
        {
            var cmd = this;
            cmd.Name = "b64";
            cmd.AddName("base64");
            cmd.Description = "Decode or encode to base64";

            var optEncode = cmd.Option("-e|--encode", "Encode to base64", CommandOptionType.NoValue);
            var optEncoding = cmd.Option<string>("-en|--encoding", "Encoding format", CommandOptionType.SingleValue);
            optEncoding.DefaultValue = "utf-8";

            var textToDecodeOrEncode = cmd.Argument<string>("textToDecodeOrEncode", "Text to decode or encode", false).IsRequired();

            cmd.OnExecute(() =>
            {
                try
                {
                    var result = string.Empty;
                    var encoding = optEncoding.ParsedValue;
                    if (optEncode.HasValue())
                    {
                        byte[] toEncodeAsBytes = Encoding.GetEncoding(encoding).GetBytes(textToDecodeOrEncode.ParsedValue);
                        result = Convert.ToBase64String(toEncodeAsBytes);
                    }
                    else
                    {
                        byte[] encodedDataAsBytes = Convert.FromBase64String(textToDecodeOrEncode.ParsedValue);
                        result = Encoding.GetEncoding(encoding).GetString(encodedDataAsBytes);
                    }

                    Console.WriteLine();
                    Console.WriteLine(result);
                    return 0;
                }
                catch (FormatException fex)
                {
                    ConsoleHelper.ExMsg(fex);
                    return 1;
                }
            });
        }
    }
}
