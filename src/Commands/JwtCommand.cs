﻿using System.IdentityModel.Tokens.Jwt;
using Antdid.Cli.Helpers;
using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;

namespace Antdid.Cli.Tools.Commands
{
    internal class JwtCommand : CliCommand
    {
        public JwtCommand()
        {
            var cmd = this;
            cmd.Name = "jwt";
            cmd.Description = "Displays the information of a jwt token";
            var handler = new JwtSecurityTokenHandler();
            var tokenJwt = cmd.Argument("tokenJwt", "Token JWT to decode", false).IsRequired();

            cmd.OnExecute(() =>
            {
                try
                {
                    var jsonSerializerOptions = JsonHelper.GetJsonSerializerOptions();
                    var jwtSecurityToken = (JwtSecurityToken)handler.ReadToken(tokenJwt.Value);

                    WriteHeader(jwtSecurityToken);
                    WritePayload(jwtSecurityToken);
                    Console.WriteLine();
                    AnsiConsole.MarkupLine($"[bold]Expiration : {jwtSecurityToken.ValidTo} [/]");
                    return 0;
                }
                catch (Exception ex)
                {
                    AnsiConsole.WriteException(ex, ExceptionFormats.ShortenEverything);
                    return 1;
                }
            });
        }

        #region Private

        private static void WriteHeader(JwtSecurityToken jwtSecurityToken)
        {
            var ruleHeader = new Rule("[red] HEADER : Algorithm & Token type [/]").LeftJustified();
            AnsiConsole.Write(ruleHeader);
            Console.Write("\r");
            ConsoleHelper.Json(jwtSecurityToken.Header.SerializeToJson());
            Console.WriteLine();
        }

        private static void WritePayload(JwtSecurityToken jwtSecurityToken)
        {
            var rulePayload = new Rule("[blue] PAYLOAD : Data [/]").LeftJustified();
            Console.Write("\r");
            AnsiConsole.Write(rulePayload);
            Console.Write("\r");
            ConsoleHelper.Json(jwtSecurityToken.Payload.SerializeToJson());
            Console.WriteLine();
        }

        #endregion Private
    }
}
