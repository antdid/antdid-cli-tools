﻿using Antdid.Cli.Tools.Commands;

namespace Antdid.Cli.Tools
{
    internal class Program
    {
        public static int Main(string[] args)
        {
            var app = Cli.Init(new CliInfo(ThisCli.Name, ThisCli.ToolCommandName, ThisCli.Version, ThisCli.Description));

            app.AddSubcommand(new GuidCommand());
            app.AddSubcommand(new JwtCommand());
            app.AddSubcommand(new DateTimeCommand());
            app.AddSubcommand(new Base64Command());
            app.AddSubcommand(new NumberConverterCommand());
            app.AddSubcommand(new TimestampCommand());

            var pluginManagerCommand = new PluginManagerCommand();
            app.AddSubcommand(pluginManagerCommand);

            foreach (var plugin in pluginManagerCommand.Plugins)
            {
                app.AddSubcommand(plugin);
            }

            return Cli.Run(app, args);
        }
    }
}
