# Antdid.Cli.Tools

.NET CLI tools

## Installation

Download the [.NET SDK](https://dotnet.microsoft.com/download/dotnet/current) or later.

Add the project registry package as a nuget source
```
dotnet nuget add source "https://gitlab.com/api/v4/projects/40800082/packages/nuget/index.json" --name "gitlab-antdid"
```

.NET Global Tool, using the command-line:

```
dotnet tool update -g antdid-cli-tools
```

## Usage

```
.NET CLI tools

Usage: adt [command] [options]

Options:
  -?|-h|--help  Show help information.

Commands:
  b64           Decode or encode to base64
  dt            Display current DateTime
  guid          Generate a guid
  jwt           Displays the information of a jwt token
  nb            Convert a number
  plugin        Managing plugins
  tt            Convert timestamp to date

Run 'adt [command] -?|-h|--help' for more information about a command.
```
