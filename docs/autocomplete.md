# Activate autocompletion

## PowerShell

To add tab completion to PowerShell for the .NET Core CLI dotnet-ad, create or edit the profile stored in the variable `$PROFILE`. For more information, see [How to create your profile](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles#how-to-create-a-profile) and [Profiles and execution policy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles#profiles-and-execution-policy).

Add the following code to your profile:

```powershell
# PowerShell parameter completion shim for the antdid-cli-tools CLI
Register-ArgumentCompleter -Native -CommandName adt -ScriptBlock {
    param($commandName, $wordToComplete)
        adt complete $wordToComplete | ForEach-Object {
            [System.Management.Automation.CompletionResult]::new($_, $_, 'ParameterValue', $_)
        }
}
```

## bash

To add tab completion to your bash shell for the .NET Core CLI, add the following code to your `.bashrc` file:
```bash
# bash parameter completion for the cli antdid-cli-tools

_adt_bash_complete()
{
  local word=${COMP_WORDS[COMP_CWORD]}

  local completions
  completions="$(adt complete "${COMP_LINE}" 2>/dev/null)"
  if [ $? -ne 0 ]; then
    completions=""
  fi

  COMPREPLY=( $(compgen -W $completions -- $word) )
}

complete -f -F _adt_bash_complete adt
```